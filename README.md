# Assignment 6: Sign Language Translator with React Framework
[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://thawing-tundra-97217.herokuapp.com/)
[Assignment 6 PDF](https://lms.noroff.no/pluginfile.php/184846/mod_assign/introattachment/0/Assignment_React_Lost%20in%20Translation.pdf?forcedownload=1)

## Summary
The application is a SPA created using the React Framework. The website contains three pages: a login page, a translation page and a profile page. To store the users and translations we used the api [API](https://github.com/dewald-els/noroff-assignment-api) created by Noroff.

### Login page
The first page to appear when running the app is the login page where the user can enter a user name after which he/she will be directed to the translation page
### Translation page
On this page the user can enter text which will be translated to sign language.
### Profile page
The profile page shows the last 10 translations the user has requested in the translation page with their translations to sign language

## Installation and usage

Go to the following [website](https://thawing-tundra-97217.herokuapp.com/)

OR 

1. clone the project repository
```sh
git clone git@gitlab.com:tiemarmepschen/assignment-6-sign-language-translator.git
```

2. Open project with VS code

3. Open the terminal in VS code and run:
```
npm install
npm start
```

## Built With
[Visual Studio Code](https://code.visualstudio.com/)

[React](https://reactjs.org/)

[Bootstrap 5](https://getbootstrap.com/)

## Credits
[Tiemar Mepschen](https://gitlab.com/tiemarmepschen)

[Lisette de Wilde](https://gitlab.com/LisettedeWilde)


## License
[MIT](https://choosealicense.com/licenses/mit/)