import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LoginView from "./views/LoginView";
import TranslationView from "./views/TranslationView"
import ProfileView from "./views/ProfileView";
import RoutePrivate from "./Utils/RoutePrivate";


function App() {
  return (
    <BrowserRouter>
      <div className="App">
        {/* helps to toggle between different routes */}
        <Routes>
          {/* add exact so that switch checks if the path matches '/  exactly*/}
          <Route path="/" exact element={<LoginView />} />
          <Route path="/translation" element={<RoutePrivate><TranslationView /></RoutePrivate>} />
          <Route path="/profile" element={<RoutePrivate><ProfileView /></RoutePrivate>} />
          <Route path="*" element={<LoginView />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
